﻿using System;

namespace Logging
{
    public interface ILogger
    {
        void Log(LogEntry entry);
    }
}
