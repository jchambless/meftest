﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MefTest
{
    public class MessageBroker : Interface.IMessageBroker
    {
        /// <summary>
        /// 
        /// </summary>
        private static MessageBroker _instance;

        /// <summary>
        /// 
        /// </summary>
        private static readonly object _locker = new object();

        /// <summary>
        /// 
        /// </summary>
        private static readonly object _subLocker = new object();

        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<Type, List<Delegate>> _subscribers;

        /// <summary>
        /// 
        /// </summary>
        public static MessageBroker Instance
        {
            get
            {
                lock (_locker)
                {
                    if (_instance == null)
                        _instance = new MessageBroker();
                    return _instance;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public MessageBroker()
        {
            this._subscribers = new Dictionary<Type, List<Delegate>>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="message"></param>
        public async void Publish<T>(object source, T message)
        {
            if (message == null || source == null)
                return;
            if (!_subscribers.ContainsKey(typeof(T)))
                return;

            var delegates = _subscribers[typeof(T)];
            if (delegates == null || delegates.Count == 0) return;
            foreach (var handler in delegates.Select(item => item as Action<T>))
            {
                await Task.Factory.StartNew(() => handler?.Invoke(message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="subscription"></param>
        public void Subscribe<T>(Action<T> subscription)
        {
            if (Monitor.TryEnter(_subLocker))
            {
                var delegates = _subscribers.ContainsKey(typeof(T)) ? _subscribers[typeof(T)] : new List<Delegate>();
                if (!delegates.Contains(subscription))
                {
                    delegates.Add(subscription);
                }
                _subscribers[typeof(T)] = delegates;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="subscription"></param>
        public void Unsubscribe<T>(Action<T> subscription)
        {
            if (Monitor.TryEnter(_subLocker))
            {
                if (!_subscribers.ContainsKey(typeof(T))) return;
                var delegates = _subscribers[typeof(T)];
                if (delegates.Contains(subscription))
                    delegates.Remove(subscription);
                if (delegates.Count == 0)
                    _subscribers.Remove(typeof(T));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            _subscribers?.Clear();
        }
    }
}
