﻿using Logging;
using log4net;

namespace MefTest
{
    public class Log4NetAdapter : ILogger
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ILog _logger = LogManager.GetLogger(typeof(Application));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entry"></param>
        public void Log(LogEntry entry)
        {
            switch(entry.Severity)
            {
                case LoggingEventType.Debug:
                    _logger.Debug(entry.Message, entry.Exception);
                    break;
                case LoggingEventType.Information:
                    _logger.Info(entry.Message, entry.Exception);
                    break;
                case LoggingEventType.Warning:
                    _logger.Warn(entry.Message, entry.Exception);
                    break;
                case LoggingEventType.Error:
                    _logger.Error(entry.Message, entry.Exception);
                    break;
                case LoggingEventType.Fatal:
                    _logger.Fatal(entry.Message, entry.Exception);
                    break;
            }
        }
    }
}
