﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logging;

namespace MefTest
{
    class Program
    {
        private static readonly ILogger logger = new Log4NetAdapter();

        static void Main(string[] args)
        {
            var modulePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Modules");
            if(!Directory.Exists(modulePath))
            {
                Directory.CreateDirectory(modulePath);
            }

            var application = new Application(modulePath, logger);
            application.Start();

            Console.ReadKey();
            application.Stop();

            System.Threading.Thread.Sleep(3000);
        }
    }
}
