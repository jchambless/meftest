﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logging;
using Interface;

namespace MefTest
{
    internal class Application : IApplicationHost
    {
        /// <summary>
        /// 
        /// </summary>
        [ImportMany(typeof(IServiceModule))]
        private IEnumerable<IServiceModule> _modules;

        /// <summary>
        /// 
        /// </summary>
        private readonly string _modulePath;

        /// <summary>
        /// 
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// 
        /// </summary>
        private readonly Interface.IMessageBroker _messageBroker;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modulePath"></param>
        public Application(string modulePath, ILogger logger)
        {
            this._modulePath = modulePath;
            this._logger = logger;
            this._messageBroker = MessageBroker.Instance;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            _logger.Info("Starting Application");

            var catalog = new AggregateCatalog();
            //catalog.Catalogs.Add(new AssemblyCatalog(typeof(Application).Assembly));
            catalog.Catalogs.Add(new DirectoryCatalog(_modulePath, "*.Module.dll"));

            var container = new CompositionContainer(catalog);
            container.ComposeExportedValue<ILogger>(_logger);
            container.ComposeExportedValue<Interface.IMessageBroker>(_messageBroker);
            container.ComposeParts(this);

            foreach(var module in _modules)
            {
                try
                {
                    module.Start();
                }
                catch(Exception ex)
                {
                    _logger.Error($"Module {module.Name}, Version {module.Version} could not start", ex);
                }
            }

            _logger.Info("Application has fully started");
        }

        /// <summary>
        /// 
        /// </summary>
        public void Stop()
        {
            _logger.Info("Stopping Application");

            foreach(var module in _modules)
            {
                try
                {
                    module.Stop();
                }
                catch(Exception ex)
                {
                    _logger.Error($"Module {module.Name}, Version {module.Version} could not stop", ex);
                }
            }

            _modules = null;
            _messageBroker.Dispose();

            _logger.Info("Application has fully stopped.");
        }
    }
}
