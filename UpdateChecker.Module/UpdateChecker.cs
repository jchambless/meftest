﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Logging;
using Interface;
using Interface.Models;

namespace UpdateChecker.Module
{
    [Export(typeof(IServiceModule))]
    public class UpdateChecker : IServiceModule
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name => "UpdateChecker";

        /// <summary>
        /// 
        /// </summary>
        public string Version => "1.0.0";

        /// <summary>
        /// 
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMessageBroker _messageBroker;

        /// <summary>
        /// 
        /// </summary>
        private readonly string _updateCheckUrl = "http://mobile.idsoftware.us/vischeck4server/AuthenticationService.svc/CheckUpdate?app=vischeck&platform=android";

        /// <summary>
        /// 
        /// </summary>
        private readonly string _partialDownloadUrl = "http://mobile.idsoftware.us/mobile/downloads/temp/";

        /// <summary>
        /// 
        /// </summary>
        private readonly TimeSpan _interval;

        /// <summary>
        /// 
        /// </summary>
        private readonly CancellationTokenSource _cancelToken;

        [ImportingConstructor]
        public UpdateChecker(ILogger logger, IMessageBroker messageBroker)
        {
            this._logger = logger;
            this._messageBroker = messageBroker;
            this._interval = TimeSpan.FromMinutes(1);    //TimeSpan.FromSeconds(30);
            this._cancelToken = new CancellationTokenSource();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            _logger.Info("Starting UpdateChecker..");

            try
            {
                _messageBroker.Subscribe<FileDownload>(HandleMessage);
                Task.Factory.StartNew(Tick, _cancelToken.Token);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Stop()
        {
            _logger.Info("Stopping UpdateChecker..");

            try
            {
                _messageBroker.Unsubscribe<FileDownload>(HandleMessage);
                _cancelToken?.Cancel();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            _logger.Info("UpdateChecker has stopped...");
        }

        /// <summary>
        /// 
        /// </summary>
        private async void Tick()
        {
            var client = new HttpClient();
            var startTime = DateTime.MinValue;

            while (true)
            {
                // Do stuff here before we cancel the thread..
                if (_cancelToken.IsCancellationRequested)
                {
                    _logger.Info("Canceling UpdateChecker...");

                    client.CancelPendingRequests();
                    client.Dispose();

                    _logger.Info("Finished.");
                    return;
                }

                if(DateTime.UtcNow - startTime > _interval)
                {
                    var httpMessage = await client.GetAsync(_updateCheckUrl, _cancelToken.Token);
                    if (httpMessage.IsSuccessStatusCode)
                    {
                        var update = await httpMessage.Content.ReadAsAsync<Update>();
                        Console.WriteLine($"Package = {update.Package}, Version = {update.VersionString}, Hash = {update.Sha1}");

                        FileDownload fileDownload = new FileDownload()
                        {
                            FileName = update.Package,
                            MimeType = "application/vnd.android.package-archive",
                            Sha1 = update.Sha1,
                            SaveTo = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Temp", update.Package),
                            DownloadFrom = new Uri(_partialDownloadUrl + update.Package, UriKind.Absolute)
                        };

                        _messageBroker.Publish(this, fileDownload);
                    }
                    startTime = DateTime.UtcNow;
                    httpMessage.Dispose();
                }
                else
                {
                    Thread.Sleep(1000);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileDownload"></param>
        private void HandleMessage(FileDownload fileDownload)
        {
            Console.WriteLine($"Currently downloading file from {fileDownload.DownloadFrom}");
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
