﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface.Models;

namespace Interface
{
    public interface ISettingsManager
    {
        bool SetSetting(Setting setting);

        Setting GetSetting(string name);

        Setting GetSetting(Guid id);
    }
}
