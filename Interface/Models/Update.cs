﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.Models
{
    public class Update
    {
        public string AppName { get; set; }
        public string Error { get; set; }
        public string Package { get; set; }
        public string Platform { get; set; }
        public string Sha1 { get; set; }
        public int VersionCode { get; set; }
        public string VersionString { get; set; }
    }
}
