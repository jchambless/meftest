﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.Models
{
    public class NotificationEvent<TObject>
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; private set; }
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime Time { get; private set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public TObject Source { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public NotificationEvent()
        {
            this.Id = Guid.NewGuid();
            this.Time = DateTime.UtcNow;
        }
    }
}
