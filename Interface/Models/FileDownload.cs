﻿using System;

namespace Interface.Models
{
    public class FileDownload
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MimeType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Uri DownloadFrom { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SaveTo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Sha1 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public FileDownload()
        {
            this.Id = Guid.NewGuid();
        }
    }
}
