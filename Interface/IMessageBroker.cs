﻿using System;

namespace Interface
{
    public interface IMessageBroker : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="message"></param>
        void Publish<T>(object source, T message);

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="subscription"></param>
        void Subscribe<T>(Action<T> subscription);

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="subscription"></param>
        void Unsubscribe<T>(Action<T> subscription);
    }
}
