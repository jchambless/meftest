﻿using System;
using System.ComponentModel.Composition;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin.Hosting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface;
using Interface.Models;
using Notification.Module.Hubs;
using Logging;

namespace Notification.Module
{
    [Export(typeof(IServiceModule))]
    public class Notification : IServiceModule
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name => "Notifications";

        /// <summary>
        /// 
        /// </summary>
        public string Version => "1.0.0";

        /// <summary>
        /// 
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// 
        /// </summary>
        private IDisposable _server;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMessageBroker _messageBroker;

        [ImportingConstructor]
        public Notification(ILogger logger, IMessageBroker messageBroker)
        {
            this._logger = logger;
            this._messageBroker = messageBroker;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            _messageBroker?.Dispose();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            _logger.Info("Starting notification module");

            try
            {
                // Change to @"http://+:8888" to connect to open up to other computers
                this._server = WebApp.Start<Startup>("http://localhost:8888");
                _messageBroker.Subscribe<NotificationEvent<FileDownload>>(HandleNotification);
            }
            catch(Exception ex)
            {
                _logger.Error("Signalr self-host could not start", ex);
            }

            _logger.Info("Notification module has started successfully");
        }

        /// <summary>
        /// 
        /// </summary>
        public void Stop()
        {
            _logger.Info("Stopping notification module");

            try
            {
                _messageBroker.Unsubscribe<NotificationEvent<FileDownload>>(HandleNotification);
                _server?.Dispose();
            }
            catch(Exception ex)
            {
                _logger.Error("Signalr self-host could not stop", ex);
            }

            _logger.Info("Notification module has stopped successfully");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notification"></param>
        private void HandleNotification(NotificationEvent<FileDownload> notification)
        {
            // Notify file download hub of download
            if (notification == null) return;


            System.Diagnostics.Trace.WriteLine("Received notification @ " + notification.Time);
        }
    }
}
