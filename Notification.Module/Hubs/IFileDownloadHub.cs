﻿using Interface.Models;

namespace Notification.Module.Hubs
{
    public interface IFileDownloadHub
    {
        void NotifyFileDownload(FileDownload fileDownload);
    }
}
