﻿using Microsoft.AspNet.SignalR;
using Interface.Models;

namespace Notification.Module.Hubs
{
    public class FileDownloadHub : Hub<IFileDownloadHub>
    {
        public void FileDownloadAvailable(FileDownload fileDownload)
        {
            Clients.All.NotifyFileDownload(fileDownload);
        }
    }
}
