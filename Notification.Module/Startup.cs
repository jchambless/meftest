﻿using Owin;
//using Microsoft.Owin.Cors;

[assembly: Microsoft.Owin.OwinStartup(typeof(Notification.Module.Startup))]
namespace Notification.Module
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //app.UseCors(CorsOptions.AllowAll);
            app.MapSignalR();
        }
    }
}
