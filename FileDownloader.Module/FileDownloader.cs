﻿using System;
using System.IO;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using Logging;
using Interface;
using Interface.Models;

namespace FileDownloader.Module
{
    [Export(typeof(IServiceModule))]
    public class FileDownloader : IServiceModule
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name => "FileDownloader";

        /// <summary>
        /// 
        /// </summary>
        public string Version => "1.0.0";

        /// <summary>
        /// 
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMessageBroker _messageBroker;

        /// <summary>
        /// 
        /// </summary>
        private readonly CancellationTokenSource _cancelToken;

        /// <summary>
        /// 
        /// </summary>
        private volatile bool _startedDownload = false;

        [ImportingConstructor]
        public FileDownloader(ILogger logger, IMessageBroker messageBroker)
        {
            this._logger = logger;
            this._messageBroker = messageBroker;
            this._cancelToken = new CancellationTokenSource();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            _logger.Info("Starting FileDownloader..");

            try
            {
                _messageBroker.Subscribe<FileDownload>(HandleMessage);
                _messageBroker.Subscribe<IEnumerable<FileDownload>>(HandleMessage);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Stop()
        {
            _logger.Info("Stopping FileDownloader..");

            try
            {
                _cancelToken?.Cancel();
                _messageBroker.Unsubscribe<FileDownload>(HandleMessage);
                _messageBroker.Unsubscribe<IEnumerable<FileDownload>>(HandleMessage);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            _logger.Info("FileDownloader has stopped...");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileDownload"></param>
        public async void HandleMessage(FileDownload fileDownload)
        {
            _logger.Info($"Entering HandleMessage for {fileDownload.FileName}");
            if (!_startedDownload && fileDownload != null)
            {
                _startedDownload = true;
                try
                {
                    HttpClient client = new HttpClient();
                    client.DefaultRequestHeaders.Clear();

                    var httpMessage = await client.GetAsync(fileDownload.DownloadFrom, _cancelToken.Token);
                    if(httpMessage.IsSuccessStatusCode)
                    {
                        using (var stream = new FileStream(fileDownload.SaveTo, FileMode.Create, FileAccess.Write, FileShare.Write))
                            await httpMessage.Content.CopyToAsync(stream);
                        bool isComplete = await ComputeHash(fileDownload.SaveTo, fileDownload.Sha1);
                        if (isComplete)
                        {
                            _logger.Info($"Successfully saved {fileDownload.SaveTo}");
                            NotificationEvent<FileDownload> notification = new NotificationEvent<FileDownload>()
                            {
                                Source = fileDownload,
                                Message = $"Successfully saved {fileDownload.SaveTo}"
                            };
                            _messageBroker.Publish(this, notification);
                        }
                        else
                        {
                            _logger.Warn($"{fileDownload.SaveTo} hash does not compute. Likely the file is corrupted.");
                            NotificationEvent<FileDownload> notification = new NotificationEvent<FileDownload>()
                            {
                                Source = fileDownload,
                                Message = $"{fileDownload.SaveTo} hash does not compute. Likely the file is corrupted."
                            };
                            _messageBroker.Publish(this, notification);
                        }
                    }
                    else
                    {
                        _logger.Info($"{fileDownload.DownloadFrom} is unavailable");
                        NotificationEvent<FileDownload> notificaton = new NotificationEvent<FileDownload>()
                        {
                            Source = fileDownload,
                            Message = $"{fileDownload.DownloadFrom} is unavailable"
                        };
                        _messageBroker.Publish(this, notificaton);
                    }

                    httpMessage.Dispose();
                    client.Dispose();
                    client = null;
                }
                catch(Exception ex)
                {
                    _logger.Error($"HandleMessage for {fileDownload.FileName} had an issue", ex);
                }
                finally
                {
                    _startedDownload = false;
                }
            }
            _logger.Info($"Leaving HandleMessage for {fileDownload.FileName}");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileList"></param>
        public async void HandleMessage(IEnumerable<FileDownload> fileList)
        {
            _logger.Info($"Entering HandleMessage for multiple downloads");

            if (fileList == null || fileList.Count() == 0)
                return;

            foreach(var fileDownload in fileList)
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Clear();
                try
                {
                    var httpMessage = await client.GetAsync(fileDownload.DownloadFrom, _cancelToken.Token);
                    if (httpMessage.IsSuccessStatusCode)
                    {
                        using (var stream = new FileStream(fileDownload.SaveTo, FileMode.Create, FileAccess.Write, FileShare.Write))
                            await httpMessage.Content.CopyToAsync(stream);
                        bool isComplete = await ComputeHash(fileDownload.SaveTo, fileDownload.Sha1);
                        if (isComplete)
                        {
                            _logger.Info($"Successfully saved {fileDownload.SaveTo}");
                        }
                        else
                        {
                            _logger.Warn($"{fileDownload.SaveTo} hash does not compute. Likely the file is corrupted.");
                        }
                    }
                    else
                    {
                        _logger.Info($"{fileDownload.DownloadFrom} is unavailable");
                    }

                    httpMessage.Dispose();
                    client.Dispose();
                    client = null;
                }
                catch(Exception ex)
                {
                    _logger.Error($"HandleMessage for multiple downloads {fileDownload.FileName} had an issue", ex);
                }
            }

            _logger.Info($"Leaving HandleMessage for multiple downloads");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="hash"></param>
        /// <returns></returns>
        private Task<bool> ComputeHash(string filePath, string hash)
        {
            return Task.Factory.StartNew(() => 
            {
                if (filePath == null || hash == null)
                    return false;
                var sha1 = SHA1.Create();
                using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    byte[] hashBytes = sha1.ComputeHash(stream);
                    string fileHash = BitConverter.ToString(hashBytes).Replace("-", "");
                    if (hash.Equals(fileHash, StringComparison.InvariantCultureIgnoreCase))
                        return true;
                }
                return false;
            }, _cancelToken.Token);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
